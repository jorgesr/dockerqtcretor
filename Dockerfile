FROM ubuntu:18.04

ENV QT_VERSION v5.13.0
ENV QT_CREATOR_VERSION v4.9.2

# Build prerequisites
RUN apt-get -y update && apt-get -y install qtbase5-dev \
	python-pip python3-pip software-properties-common \
	libxcb-xinerama0-dev build-essential python \
	libnlopt-dev tmux wget zip git vim \
	zlib1g-dev cmake
	
RUN add-apt-repository ppa:ubuntugis/ubuntugis-unstable
RUN apt-get install -y libgsl-dev libgdal-dev libevent-dev python-dev build-essential libgdal-dev python-gdal
RUN pip install psycopg2 owslib --upgrade

RUN ln -s /usr/lib/x86_64-linux-gnu/libnlopt.so /usr/local/lib/libnlopt.so

# Other useful tools
RUN apt-get -y update && apt-get -y install 
# Simple root password in case we want to customize the container
RUN echo "root:root" | chpasswd

RUN useradd -G video -ms /bin/bash user
RUN mkdir -p /qt/build
WORKDIR /qt/build
ADD build_qt.sh /qt/build/build_qt.sh
RUN QT_VERSION=$QT_VERSION QT_CREATOR_VERSION=$QT_CREATOR_VERSION /qt/build/build_qt.sh

USER user

WORKDIR /qt

CMD ["bash"]
